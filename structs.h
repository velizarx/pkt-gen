#include <stdio.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>
#include <ctype.h>	// isprint()
#include <unistd.h>	// sysconf()
#include <sys/poll.h>
#include <arpa/inet.h>	/* ntohs */
#include <ifaddrs.h>	/* getifaddrs */
#include <pthread.h>

#ifndef NO_PCAP
#include <pcap/pcap.h>
#endif

struct ip_data {
	char *name;
	struct range *ranges;
	int count;
	int type;
};
enum r_type { IP_GEOIP_FILE, IP_RANGE, IP_LIST, IP_LIST_FILE };
#define MAX_IP_RANGE_STRLEN 33

struct range {
  uint32_t start, end; /* same as struct in_addr */
};

struct mac_range {
	char *name;
	struct ether_addr start, end;
};

#ifdef linux
#define cpuset_t        cpu_set_t
#define ifr_flagshigh  ifr_flags        /* only the low 16 bits here */

#define IFF_PPROMISC   IFF_PROMISC      /* IFF_PPROMISC does not exist */
#include <linux/ethtool.h>
#include <linux/sockios.h>

#define CLOCK_REALTIME_PRECISE CLOCK_REALTIME
#include <netinet/ether.h>      /* ether_aton */
#include <linux/if_packet.h>    /* sockaddr_ll */
#endif  /* linux */



#define SKIP_PAYLOAD 1 /* do not check payload. XXX unused */
#define VIRT_HDR_1	10	/* length of a base vnet-hdr */
#define VIRT_HDR_2	12	/* length of the extenede vnet-hdr */
#define VIRT_HDR_MAX	VIRT_HDR_2

struct virt_header {
	uint8_t fields[VIRT_HDR_MAX];
};

/* ifname can be netmap:foo-xxxx */
#define MAX_IFNAMELEN	64	/* our buffer for ifname */
#define MAX_SRC_PORTS 50
//#define MAX_PKTSIZE	1536
#define MAX_PKTSIZE	MAX_BODYSIZE	/* XXX: + IP_HDR + ETH_HDR */

/* compact timestamp to fit into 60 byte packet. (enough to obtain RTT) */
struct tstamp {
	uint32_t sec;
	uint32_t nsec;
};

/* counters to accumulate statistics */
struct my_ctrs {
	uint64_t pkts, bytes, events;
	struct timeval t;
};

/* set the thread affinity. */
static int
setaffinity(pthread_t me, int i)
{
	cpuset_t cpumask;

	if (i == -1)
		return 0;

	/* Set thread affinity affinity.*/
	CPU_ZERO(&cpumask);
	CPU_SET(i, &cpumask);

	if (pthread_setaffinity_np(me, sizeof(cpuset_t), &cpumask) != 0) {
		D("Unable to set affinity: %s", strerror(errno));
		return 1;
	}
	return 0;
}

/*
 * Arguments for a new thread. The same structure is used by
 * the source and the sink
 */
struct targ {
	struct glob_arg *g;
	int used;
	int completed;
	int cancel;
	int fd;
	struct nm_desc *nmd;
	/* these ought to be volatile, but they are
	 * only sampled and errors should not accumulate
	 */
	struct my_ctrs ctr;

	struct timespec tic, toc;
	int me;
	pthread_t thread;
	int affinity;

	struct pkt pkt;
	void *frame;
	int pkt_size;
	int tic_cng_payload;
	int tic_cng_ip_data;
};

#ifdef __linux__
#define sockaddr_dl    sockaddr_ll
#define sdl_family     sll_family
#define AF_LINK        AF_PACKET
#define LLADDR(s)      s->sll_addr;
#include <linux/if_tun.h>
#define TAP_CLONEDEV	"/dev/net/tun"
#endif /* linux */

/*
 * global arguments for all threads
 */
struct glob_arg {
	struct ip_data src_ip;
	struct ip_data dst_ip;
	struct mac_range dst_mac;
	struct mac_range src_mac;
	int pkt_size;
	int max_paylen_size;
	int burst;
	int npackets;	/* total packets to send */
	int frags;	/* fragments per packet */
	int nthreads;
	int cpus;	/* cpus used for running */
	int system_cpus;	/* cpus on the system */
	int options;	/* testing */
#define OPT_PREFETCH	1
#define OPT_ACCESS	2
#define OPT_COPY	4
#define OPT_MEMCPY	8
#define OPT_TS		16	/* add a timestamp */
#define OPT_PAYLOAD_FILE	32
#define OPT_DUMP	64	/* dump rx/tx traffic */
#define OPT_RUBBISH	256	/* send wathever the buffers contain */
#define OPT_RANDOM_SRC  512
#define OPT_RANDOM_DST  1024
#define OPT_SRC_PORT_LIST 2048
#define OPT_DST_PORT_LIST 4096
#define OPT_RANDOM_SRC_PORT 8192
#define OPT_RANDOM_DST_PORT 16384
#define OPT_RANDOM_PAYLOAD 32768
#define OPT_RANDOM_PAYLOAD_LEN 65536
	int dev_type;
#ifndef NO_PCAP
	pcap_t *p;
#endif

	int tx_rate;
	struct timespec tx_period;
	int affinity;
	int main_fd;
	struct nm_desc *nmd;
	int report_interval;		/* milliseconds between prints */
	void *(*td_body)(void *);
	void *mmap_addr;
	char ifname[MAX_IFNAMELEN];
	char *nmr_config;
	int dummy_send;
	int extra_bufs;		/* goes in nr_arg3 */
	int extra_pipes;	/* goes in nr_arg1 */
	int wait_link;
	char *payload_file;
	uint8_t *payload_data;
	long int payload_data_len;
	// Ports list
	int *src_ports;
	int count_src_ports;
	int *dst_ports;
	int count_dst_ports;
	// tcp
	char *tcp_flags;
	// icmp
	uint8_t icmp_type;
	uint8_t icmp_code;
};
enum dev_type { DEV_NONE, DEV_NETMAP, DEV_PCAP, DEV_TAP };

#ifdef __linux__
#define l4h_sport source
#define l4h_dport dest
#define l4h_ulen len
#define l4h_sum check
#endif /* linux */

// Global
static struct targ *targs;
static int global_nthreads;
int verbose = 0;
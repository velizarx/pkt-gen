# For multiple programs using a single source file each,
# we can just define 'progs' and create custom targets.
PROGS	=	tcp-gen icmp-gen udp-gen
TEST    =   test-fileread

CLEANFILES = $(PROGS) $(TEST) *.o
NO_MAN=
CFLAGS = -O2 -pipe
CFLAGS += -Werror -Wall #-Wno-unused-function #-Wno-unused-variable
CFLAGS += -I ./ -I ./sys
CFLAGS += -Wextra -DNO_PCAP


LDFLAGS += -lpthread
LDFLAGS += -lrt	# needed on linux, does not harm on BSD

all: $(PROGS)


tcp-gen: tcp-gen.o
	$(CC) $(CFLAGS) -o tcp-gen tcp-gen.o $(LDFLAGS)

icmp-gen: icmp-gen.o
	$(CC) $(CFLAGS) -o icmp-gen icmp-gen.o $(LDFLAGS)

udp-gen: udp-gen.o
	$(CC) $(CFLAGS) -o udp-gen udp-gen.o $(LDFLAGS)

test: test-fileread.o
	$(CC) $(CFLAGS) -o test-fileread test-fileread.o $(LDFLAGS)

clean:
	-@rm -rf $(CLEANFILES)

#define _GNU_SOURCE	/* for CPU_SET() */
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#define MAX_BODYSIZE	16384
struct pkt {
	struct ether_header eh;
	struct ip ip;
	struct udphdr udp;
	uint8_t body[MAX_BODYSIZE];	// XXX hardwired
} __attribute__((__packed__));

#include <structs.h>

const long int SIZE_UDP_HDR = sizeof(struct udphdr);
const long int SIZE_IP_HDR = sizeof(struct ip);
const long int SIZE_ET_HDR = sizeof(struct ether_header);

static void change_packet(struct pkt *pkt, struct targ *t);
static void initialize_packet(struct targ *targ);
#define PROTO_UDP
const char *cmd = "udp-gen";

#include <functions.h>

/*
 * increment the addressed in the packet,
 * starting from the least significant field.
 *	DST_IP DST_PORT SRC_IP SRC_PORT
 */
static void change_packet(struct pkt *pkt, struct targ *t)
{
	struct ip *ip = &pkt->ip;
	struct udphdr *l4hdr = &pkt->udp;
	uint16_t paylen = ntohs(l4hdr->l4h_ulen) - SIZE_UDP_HDR;
	int ip_size;

	if (t->g->tx_rate == 0 && t->tic_cng_ip_data != 10) {
		t->tic_cng_ip_data++;
		goto payload;
	}
	// Update source and destination ip
	update_ip(t, ip);
	// Update ports
	if (t->g->options & OPT_RANDOM_SRC_PORT) {
		l4hdr->l4h_sport = random();
	} else if (t->g->options & OPT_SRC_PORT_LIST) {
		l4hdr->l4h_sport = htons(t->g->src_ports[rand() % t->g->count_src_ports]);
	}
	if (t->g->options & OPT_RANDOM_DST_PORT) {
		l4hdr->l4h_dport = random();
	} else if (t->g->options & OPT_DST_PORT_LIST) {
		l4hdr->l4h_dport = htons(t->g->dst_ports[rand() % t->g->count_dst_ports]);
	} 

payload:
	if (t->g->options & OPT_RANDOM_PAYLOAD) {
		if (t->tic_cng_payload != 100) {
			t->tic_cng_payload++;
			goto checksum;
		}
		if (t->g->options & OPT_RANDOM_PAYLOAD_LEN) {
			paylen = rand() % t->g->max_paylen_size;
			ip_size = paylen + SIZE_IP_HDR + SIZE_UDP_HDR;
			ip->ip_len = ntohs(ip_size);
			t->pkt_size = ip_size + SIZE_ET_HDR;
			l4hdr->l4h_ulen = htons(paylen + SIZE_UDP_HDR);
			//D("Thread #%d new paylen %d udphdr %ld iphdr %ld ethdr %ld pkt_size %d", t->me, paylen, SIZE_UDP_HDR, SIZE_IP_HDR, SIZE_ET_HDR, t->pkt_size);
		}
		// Fill random payload
		fill_rand_payload(pkt, (rand() + l4hdr->l4h_sum), paylen);
		t->tic_cng_payload = 0;
	}

checksum:
	// update checksum IP
	ip->ip_sum = 0; // zero previosly checksum
	ip->ip_sum = wrapsum(checksum(ip, sizeof(*ip), 0));
	l4hdr->l4h_sum = 0;
	l4hdr->l4h_sum = wrapsum(checksum(l4hdr, sizeof(*l4hdr),
	            checksum(pkt->body,
	                paylen,
	                checksum(&ip->ip_src, 2 * sizeof(ip->ip_src),
	                    IPPROTO_UDP + paylen + SIZE_UDP_HDR
	                )
	            )
	        ));
}

/*
 * initialize one packet and prepare for the next one.
 * The copy could be done better instead of repeating it each time.
 */
static void
initialize_packet(struct targ *targ)
{
	struct pkt *pkt = &targ->pkt;
	struct ether_header *eh;
	struct ip *ip;
	struct udphdr *udp;
	uint16_t paylen = targ->pkt_size - SIZE_ET_HDR - SIZE_IP_HDR - SIZE_UDP_HDR;

	if (targ->g->options & OPT_PAYLOAD_FILE) {
		paylen = targ->g->payload_data_len;
		bzero(pkt->body, MAX_BODYSIZE);
		memcpy(pkt->body, targ->g->payload_data, paylen);
		targ->pkt_size = paylen + SIZE_ET_HDR + SIZE_IP_HDR + SIZE_UDP_HDR;
	}
	targ->g->max_paylen_size = paylen;
	if(verbose)
		D("Thread %d max paylen_size %d UDP_HDR %ld ET_IP_HDR %ld", targ->me, paylen, SIZE_UDP_HDR, SIZE_ET_HDR + SIZE_IP_HDR);

	if (targ->g->options & OPT_RANDOM_PAYLOAD)
		fill_rand_payload(pkt, time(NULL), paylen);

	/* prepare the headers */
	// IP header
	ip = &pkt->ip;
	initialize_iphdr(targ, ip, IPPROTO_UDP);
	// UPD header
	udp = &pkt->udp;
	udp->l4h_sport = 0;//random();
	udp->l4h_dport = htons(161);//random();
	udp->l4h_ulen = htons(paylen + SIZE_UDP_HDR);
	// Magic: taken from sbin/dhclient/packet.c
	udp->l4h_sum = wrapsum(checksum(udp, sizeof(*udp),
	            checksum(pkt->body,
	                paylen,
	                checksum(&ip->ip_src, 2 * sizeof(ip->ip_src),
	                    IPPROTO_UDP + paylen + SIZE_UDP_HDR
	                )
	            )
	        ));
	// Ethernet header
	eh = &pkt->eh;
	initialize_ehhdr(targ, eh);

	if (targ->g->options & OPT_DUMP) {
		printf("Dump payload in initialize_packet()\n");
		dump_payload((void *)pkt, targ->g->pkt_size, NULL, 0);
	}
}


int
main(int arc, char **argv)
{
	struct glob_arg g;

	bzero(&g, sizeof(g));

	arg_parse(&g, arc,argv);

	/* Install ^C handler. */
	global_nthreads = g.nthreads;
	signal(SIGINT, sigint_h);

	start_threads(&g);
	main_thread(&g);

	free(g.payload_data);
	free(g.src_ip.ranges);
	free(g.dst_ip.ranges);
	free(g.src_ports);
	free(g.dst_ports);
	return 0;
}
#define _GNU_SOURCE	/* for CPU_SET() */
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

#define MAX_BODYSIZE	16384
struct pkt {
	struct ether_header eh;
	struct ip ip;
	struct icmp icmp;
	uint8_t body[MAX_BODYSIZE];	// XXX hardwired
} __attribute__((__packed__));

#include <structs.h>

static void change_packet(struct pkt *pkt, struct targ *t);
static void initialize_packet(struct targ *targ);
#define PROTO_ICMP
const char *cmd = "icmp-gen";

#include <functions.h>


/*
 * increment the addressed in the packet,
 * starting from the least significant field.
 *	DST_IP DST_PORT SRC_IP SRC_PORT
 */
static void change_packet(struct pkt *pkt, struct targ *t)
{
	struct ip *ip = &pkt->ip;
	struct icmp *l4hdr = &pkt->icmp;

	if (t->g->tx_rate == 0 && t->tic_cng_ip_data != 10) {
		t->tic_cng_ip_data++;
		goto checksum;
	}
	// Update source and destination ip
	update_ip(t, ip);

checksum:
	// update checksum IP
	ip->ip_sum = 0; // zero previosly checksum
	ip->ip_sum = wrapsum(checksum(ip, sizeof(*ip), 0));
	l4hdr->icmp_hun.ih_idseq.icd_seq = ntohl(rand()); // Contains the sequence number.
	// update checksum
	l4hdr->icmp_cksum = 0; // zero previosly checksum
	l4hdr->icmp_cksum = wrapsum(checksum(l4hdr, sizeof(*l4hdr), 0));
}

/*
 * initialize one packet and prepare for the next one.
 * The copy could be done better instead of repeating it each time.
 */
static void
initialize_packet(struct targ *targ)
{
	struct pkt *pkt = &targ->pkt;
	struct ether_header *eh;
	struct ip *ip;
	struct icmp *icmp;

	// Fix error: ‘fill_rand_payload’ defined but not used
	if (targ->g->options & OPT_RANDOM_PAYLOAD)
		fill_rand_payload(pkt, time(NULL), 0);

	pkt->body[0] = '\0';
	/* prepare the headers */
	// IP header
	ip = &pkt->ip;
	initialize_iphdr(targ, ip, IPPROTO_ICMP);
	// ICMP header
	icmp = &pkt->icmp;
	icmp->icmp_type = targ->g->icmp_type;
	icmp->icmp_code = targ->g->icmp_code;
	icmp->icmp_cksum = wrapsum(checksum(icmp, sizeof(*icmp), 0));
	// Ethernet header
	eh = &pkt->eh;
	initialize_ehhdr(targ, eh);

	if (targ->g->options & OPT_DUMP) {
		printf("Dump payload in initialize_packet()\n");
		dump_payload((void *)pkt, targ->g->pkt_size, NULL, 0);
	}
}


int
main(int arc, char **argv)
{
	struct glob_arg g;

	bzero(&g, sizeof(g));

	arg_parse(&g, arc,argv);

	/* Install ^C handler. */
	global_nthreads = g.nthreads;
	signal(SIGINT, sigint_h);

	start_threads(&g);
	main_thread(&g);

	free(g.src_ip.ranges);
	free(g.dst_ip.ranges);
	return 0;
}

/* end of file */
